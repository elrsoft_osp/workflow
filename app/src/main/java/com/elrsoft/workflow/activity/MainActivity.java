package com.elrsoft.workflow.activity;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.fragment.MainFragment;


public class MainActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            FragmentNavigationManagerController.navigationManagerControlle(this, new MainFragment(), getString(R.string.main));
        }

    }


    @Override
    public void onBackPressed() {
        MainFragment myFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.main));
        if (myFragment != null && myFragment.isVisible()) {
            super.onBackPressed();
        } else {
            FragmentNavigationManagerController.navigationManagerControlle(this, new MainFragment(), getString(R.string.main));
        }

    }

}
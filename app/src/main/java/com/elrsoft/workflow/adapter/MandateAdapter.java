package com.elrsoft.workflow.adapter;

import android.animation.ValueAnimator;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.fragment.MainFragment;
import com.elrsoft.workflow.model.Mandate;

import java.util.List;

/**
 * Created by misha on 12/03/15.
 */
public class MandateAdapter extends RecyclerView.Adapter<MandateAdapter.ViewHolder> {

    LayoutInflater inflater;
    List<Mandate> list;
    MainFragment fragment;
    OnItemLongClickListener onItemClickListener;

    public MandateAdapter(MainFragment fragment, List<Mandate> list) {
        this.fragment = fragment;
        this.list = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.from(parent.getContext()).inflate(R.layout.mandate_item, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Mandate mandate = list.get(position);
        if (mandate != null) {
            if (mandate.getStatus().getName().equals("Виконано")) {
                holder.dateSuccess.setText("" + DateFormat.format("dd/MM/yyyy", mandate.getDateSuccess()));
                holder.card_view.setCardBackgroundColor(fragment.getResources().getColor(R.color.green));
            } else if (mandate.getStatus().getName().equals("Не виконано")) {
                holder.card_view.setCardBackgroundColor(fragment.getResources().getColor(R.color.red));
            } else {
                holder.card_view.setCardBackgroundColor(fragment.getResources().getColor(R.color.yel));
            }
            holder.lastName.setText(mandate.getUser().getName());
            holder.thema.setText(mandate.getTheme().getTopic());
            holder.dateCreate.setText("" + DateFormat.format("dd/MM/yyyy", mandate.getDateCreate()));
            holder.dateCompleted.setText("" + DateFormat.format("dd/MM/yyyy", mandate.getDateCompleted()));

            holder.comment.setText(mandate.getDescription());
            holder.status.setText(mandate.getStatus().getName());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemLongClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private int originalHeight = 0;
        private boolean isViewExpanded = false;
        boolean mIsViewExpanded;

        TextView lastName, status, dateSuccess,  thema, dateCompleted, dateCreate, comment;
        LinearLayout conteinerAnim;
        CardView card_view;


        public ViewHolder(final View itemView) {
            super(itemView);

            card_view = (CardView) itemView.findViewById(R.id.card_view);
            lastName = (TextView) itemView.findViewById(R.id.txtlastName);
            thema = (TextView) itemView.findViewById(R.id.txtThema);
            dateCreate = (TextView) itemView.findViewById(R.id.txtDateCreate);
            dateCompleted = (TextView) itemView.findViewById(R.id.txtDateCompleted);
            status = (TextView) itemView.findViewById(R.id.status);

            dateSuccess = (TextView) itemView.findViewById(R.id.dateSuccess);

            comment = (TextView) itemView.findViewById(R.id.comment);
            conteinerAnim = (LinearLayout) itemView.findViewById(R.id.container_anim);
            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // If the originalHeight is 0 then find the height of the View being used
                    // This would be the height of the cardview
                    if (originalHeight == 0) {
                        originalHeight = itemView.getHeight();
                    }


                    // Declare a ValueAnimator object
                    ValueAnimator valueAnimator;
                    if (!mIsViewExpanded) {
                        conteinerAnim.setVisibility(View.VISIBLE);
                        conteinerAnim.setEnabled(true);
                        mIsViewExpanded = true;
                        valueAnimator = ValueAnimator.ofInt(originalHeight, originalHeight + (int) (originalHeight * 2.0)); // These values in this method can be changed to expand however much you like
                    } else {

                        mIsViewExpanded = false;
                        valueAnimator = ValueAnimator.ofInt(originalHeight + (int) (originalHeight * 2.0), originalHeight);

                        Animation a = new AlphaAnimation(1.00f, 0.00f); // Fade out

                        a.setDuration(200);
                        // Set a listener to the animation and configure onAnimationEnd
                        a.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                conteinerAnim.setVisibility(View.INVISIBLE);
                                conteinerAnim.setEnabled(false);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        // Set the animation on the custom view
                        conteinerAnim.startAnimation(a);
                    }
                    valueAnimator.setDuration(200);
                    valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator animation) {
                            Integer value = (Integer) animation.getAnimatedValue();
                            itemView.getLayoutParams().height = value;
                            itemView.requestLayout();
                        }
                    });


                    valueAnimator.start();
                }
            });

            if (!isViewExpanded) {
                // Set Views to View.GONE and .setEnabled(false)
                conteinerAnim.setVisibility(View.GONE);
                conteinerAnim.setEnabled(false);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onLongClick(list.get(getPosition()));
            }
            return true;
        }
    }

    public interface OnItemLongClickListener {
        public void onLongClick(Mandate mandate);
    }
}

package com.elrsoft.workflow.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.elrsoft.workflow.R;

import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 13.03.2015.
 */
public class SpinnerAdapter extends BaseAdapter {

    List<String> userModelList;
    LayoutInflater inflater;

    private class ViewHolder {
        TextView name;
    }

    public SpinnerAdapter(List<String> userModelList, Activity activity) {
        this.userModelList = userModelList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return userModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return userModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.spinner_adapter_item, null, false);
            holder.name = (TextView) view.findViewById(R.id.name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        String name = (String) getItem(position);
        if (name != null) {
            holder.name.setText(name);
        }
        return view;
    }
}

package com.elrsoft.workflow.controller;

import android.os.Bundle;

import com.elrsoft.workflow.model.Mandate;

import java.util.ArrayList;

/**
 * Created by <ELRsoft> HANZ on 14.03.2015.
 */
public class BundleManagerController {

    public static Bundle createBundleWithMandate(Mandate mandate, String key) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(key, mandate);
        return bundle;
    }

    public static Bundle createBundleWithMandateList(ArrayList<Mandate> mandate, String key) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(key, mandate);
        return bundle;
    }
}

package com.elrsoft.workflow.controller;

import com.elrsoft.workflow.db.Resource;
import com.elrsoft.workflow.model.Status;
import com.elrsoft.workflow.model.Theme;
import com.elrsoft.workflow.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 15.03.2015.
 */
public class ConvertController {

    public static List<String> convertUserToString(List<User> users) {
        List<String> stringList = new ArrayList<>();
        stringList.add("");
        for (User user : users) {
            stringList.add(user.getName());
        }
        return stringList;
    }

    public static List<String> convertThemeToString(List<Theme> themes) {
        List<String> stringList = new ArrayList<>();
        stringList.add("");
        for (Theme theme : themes) {
            stringList.add(theme.getTopic());
        }
        return stringList;
    }

    public static List<String> convertStatusToString(List<Status> statuses) {
        List<String> statusList = new ArrayList<>();
        statusList.add("");
        for (Status status : statuses) {
            statusList.add(status.getName());
        }
        return statusList;
    }

}

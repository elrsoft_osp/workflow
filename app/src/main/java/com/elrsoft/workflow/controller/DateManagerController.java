package com.elrsoft.workflow.controller;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by <ELRsoft> HANZ on 16.03.2015.
 */
public class DateManagerController {

    public static Date getDateInNextDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

}

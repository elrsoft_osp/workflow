package com.elrsoft.workflow.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.elrsoft.workflow.R;

/**
 * Created by misha on 11/03/15.
 */
public class FragmentNavigationManagerController {

    public static void navigationManagerControlle(FragmentActivity fragmentActivity, Fragment newFragment, String nameFragment) {
        FragmentManager manager = fragmentActivity.getSupportFragmentManager();
        manager.beginTransaction().setCustomAnimations(R.anim.in, R.anim.out).replace(R.id.container, newFragment, nameFragment).commit();
    }
}

package com.elrsoft.workflow.controller;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 13.03.2015.
 */
public class ResourceController {

    public static List<String> getAllStatus() {
        List<String> list = new ArrayList<>();
        list.add("Виконано");
        list.add("Не виконано");
        list.add("В роботі");
        return list;
    }

    public static void toast(Context context,String toast){
        Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
    }

    public static void toast(Context context,int toast){
        Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
    }

}

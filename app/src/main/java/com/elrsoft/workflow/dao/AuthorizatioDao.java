package com.elrsoft.workflow.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.elrsoft.workflow.db.DBOperation;
import com.elrsoft.workflow.db.Resource;
import com.elrsoft.workflow.model.Authorization;
import com.elrsoft.workflow.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by misha on 12/03/15.
 */
public class AuthorizatioDao implements Dao<Authorization> {

    Context context;

    @Override
    public long save(Authorization authorization) {
        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            return DBOperation.sqLiteDatabase.insert(Resource.Auth.TABLE_NAME, null, fullUserContentValue(authorization));
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public long delete(Authorization authorization) {
        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            return DBOperation.sqLiteDatabase.delete(Resource.Auth.TABLE_NAME, Resource.Auth.ID + " = ? ",
                    new String[]{String.valueOf(authorization.getId())
                    }
            );
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public int update(Authorization authorization) {
        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            return DBOperation.sqLiteDatabase.update(Resource.Auth.TABLE_NAME, fullUserContentValue(authorization), Resource.Auth.ID + "= ?",
                    new String[]{String.valueOf(authorization.getId())});
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public List<Authorization> get() {
        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Auth.TABLE_NAME, null);
            List<Authorization> authList = parseCursor(cursor);
            return authList;
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public Authorization getBy(long id) {

        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Auth.TABLE_NAME + " where " + Resource.Auth.ID + "= ?", new String[]{String.valueOf(id)});
            if (cursor != null) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }

        } finally {
            DBOperation.close();
        }
    }

    @Override
    public List<Authorization> parseCursor(Cursor cursor) {
        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            List<Authorization> authList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(cursor.getColumnIndex(Resource.Auth.ID));
                    String login = cursor.getString(cursor.getColumnIndex(Resource.Auth.LOGIN));
                    String password = cursor.getString(cursor.getColumnIndex(Resource.Auth.PASSWORD));
                    User user = new User(cursor.getLong(cursor.getColumnIndex(Resource.Auth.USER_ID)));
                    authList.add(new Authorization(id, login, password, user));

                } while (cursor.moveToNext());

            }
            if (cursor != null) {
                cursor.close();
            }
            return authList;
        } finally {
            DBOperation.close();
        }
    }

    private ContentValues fullUserContentValue(Authorization authorization) {
        ContentValues cv = new ContentValues();
        cv.put(Resource.Auth.ID, authorization.getId());
        cv.put(Resource.Auth.LOGIN, authorization.getLogin());
        cv.put(Resource.Auth.PASSWORD, authorization.getPassword());
        cv.put(Resource.Auth.USER_ID, String.valueOf(authorization.getUser()));
        return cv;
    }
}
package com.elrsoft.workflow.dao;

import android.database.Cursor;

import com.elrsoft.workflow.model.User;

import java.util.List;

/**
 * Created by misha on 12/03/15.
 */
public interface Dao<T> {

    long save(T t);

    long delete(T t);

    int update(T t);

    List<T> get();

    T getBy(long id);

    List<T> parseCursor(Cursor cursor);
}
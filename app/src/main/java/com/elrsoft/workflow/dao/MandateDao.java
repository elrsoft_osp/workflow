package com.elrsoft.workflow.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.elrsoft.workflow.db.DBOperation;
import com.elrsoft.workflow.db.Resource;
import com.elrsoft.workflow.model.Mandate;
import com.elrsoft.workflow.model.Status;
import com.elrsoft.workflow.model.Theme;
import com.elrsoft.workflow.model.User;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by misha on 12/03/15.
 */
public class MandateDao implements Dao<Mandate> {
    Context context;

    public MandateDao(Context context) {
        this.context = context;
    }

    @Override
    public long save(Mandate mandate) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.insert(Resource.Mandate.TABLE_NAME, null, fullUserContentValue(mandate));
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public long delete(Mandate mandate) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.delete(Resource.Mandate.TABLE_NAME, Resource.Mandate.ID + " = ?",
                    new String[]{String.valueOf(mandate.getId())});
        } finally {
            DBOperation.close();
        }
    }


    @Override
    public int update(Mandate mandate) {
        try {
            DBOperation.open(context);
            Log.d("MyLog", " id " + mandate.getId());
            return DBOperation.sqLiteDatabase.update(Resource.Mandate.TABLE_NAME, fullUserContentValueUpdate(mandate), Resource.Mandate.ID + " = ?",
                    new String[]{String.valueOf(mandate.getId())});
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public List<Mandate> get() {
        try {
            DBOperation.open(context);

            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Mandate.TABLE_NAME + ", "
                    + Resource.User.TABLE_NAME + ", " + Resource.Status.TABLE_NAME + ", " + Resource.Theme.TABLE_NAME + " where " + Resource.Mandate.USER_ID
                    + "= " + Resource.User.TABLE_NAME + "." + Resource.User.ID + " AND " + Resource.Mandate.STATUS + " = "
                    + Resource.Status.TABLE_NAME + "." + Resource.Status.ID + " AND " + Resource.Mandate.THEME + " = "
                    + Resource.Theme.TABLE_NAME + "." + Resource.Theme.ID, null);
            List<Mandate> userList = parseCursor(cursor);
            return userList;
        } finally {
            DBOperation.close();
        }
    }

    public List<Mandate> getMandateWithSearge(String nameUser, String date, String topic, String status) {
        try {
            DBOperation.open(context);

            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Mandate.TABLE_NAME + ", "
                    + Resource.User.TABLE_NAME + ", " + Resource.Status.TABLE_NAME +", " + Resource.Theme.TABLE_NAME +  " where " + Resource.Mandate.USER_ID
                    + "= " + Resource.User.TABLE_NAME + "." + Resource.User.ID + " AND " + Resource.Mandate.STATUS + " = "
                    + Resource.Status.TABLE_NAME + "." + Resource.Status.ID + " AND " + Resource.Mandate.THEME + " = "
                    + Resource.Theme.TABLE_NAME + "." + Resource.Theme.ID + (nameUser != null ? " AND " + Resource.User.NAME
                    + " = " + "'" + nameUser + "'" : "")
                    + (date != null ? " AND ((" + Resource.Mandate.DATE_CREATE + " = " + "'" + date + "'"
                    + ") OR (" + Resource.Mandate.DATE_COMPLETED + " = " + "'" + date + "'"
                    + ") OR (" + Resource.Mandate.DATE_SUCCESS + " = " + "'" + date + "'" + "))" : "")
                    + (topic != null ? " AND " + Resource.Theme.TOPIC
                    + " = " + "'" + topic + "'" : "") + (status != null ? " AND " + Resource.Status.NAME
                    + " = " + "'" + status + "'" : ""), null);
            List<Mandate> userList = parseCursor(cursor);
            return userList;
        } finally {
            DBOperation.close();
        }
    }

    public List<Mandate> getFilter(String dateCreate, String dateComplite, String userName) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Mandate.TABLE_NAME + ", "
                    + Resource.User.TABLE_NAME + ", " + Resource.Status.TABLE_NAME +", " + Resource.Theme.TABLE_NAME +  " where " + Resource.Mandate.USER_ID
                    + "= " + Resource.User.TABLE_NAME + "." + Resource.User.ID + " AND " + Resource.Mandate.STATUS + " = "
                    + Resource.Status.TABLE_NAME + "." + Resource.Status.ID + " AND " + Resource.Mandate.THEME + " = "
                    + Resource.Theme.TABLE_NAME + "." + Resource.Theme.ID +
                    (dateCreate != null ? " AND ((" + Resource.Mandate.DATE_CREATE + " >= " + "'" + dateCreate + "'"
                            + ") OR (" + Resource.Mandate.DATE_COMPLETED + " >= " + "'" + dateCreate + "'"
                            + ") OR (" + Resource.Mandate.DATE_SUCCESS + " >= " + "'" + dateCreate + "'" + "))" : "")
                    + (dateComplite != null ? " AND ((" + Resource.Mandate.DATE_CREATE + " <= " + "'" + dateComplite + "'"
                    + ") OR (" + Resource.Mandate.DATE_COMPLETED + " <= " + "'" + dateComplite + "'"
                    + ") OR (" + Resource.Mandate.DATE_SUCCESS + " <= " + "'" + dateComplite + "'" + "))" : "")
                    + (userName != null ? " AND " + Resource.User.NAME + " = " + "'" + userName + "'" : ""), null);
            List<Mandate> userList = parseCursor(cursor);
            return userList;
        } finally {
            DBOperation.close();
        }
    }

    public List<Mandate> getFilterNextDay(String dateCreate, String dateComplite) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Mandate.TABLE_NAME + ", "
                    + Resource.User.TABLE_NAME + ", " + Resource.Status.TABLE_NAME +", " + Resource.Theme.TABLE_NAME +  " where " + Resource.Mandate.USER_ID
                    + "= " + Resource.User.TABLE_NAME + "." + Resource.User.ID + " AND " + Resource.Mandate.STATUS + " = "
                    + Resource.Status.TABLE_NAME + "." + Resource.Status.ID + " AND " + Resource.Mandate.THEME + " = "
                    + Resource.Theme.TABLE_NAME + "." + Resource.Theme.ID +
                    (dateCreate != null ? " AND " + Resource.Mandate.DATE_COMPLETED + " >= " + "'" + dateCreate + "'" : "")
                    + (dateComplite != null ? " AND " + Resource.Mandate.DATE_COMPLETED + " <= " + "'" + dateComplite + "'" : "")
                    , null);
            List<Mandate> userList = parseCursor(cursor);
            return userList;
        } finally {
            DBOperation.close();
        }
    }

    public List<Mandate> getFilterToday(String dateCreate, String dateComplite) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Mandate.TABLE_NAME + ", "
                    + Resource.User.TABLE_NAME + ", " + Resource.Status.TABLE_NAME +", " + Resource.Theme.TABLE_NAME +  " where " + Resource.Mandate.USER_ID
                    + "= " + Resource.User.TABLE_NAME + "." + Resource.User.ID + " AND " + Resource.Mandate.STATUS + " = "
                    + Resource.Status.TABLE_NAME + "." + Resource.Status.ID + " AND " + Resource.Mandate.THEME + " = "
                    + Resource.Theme.TABLE_NAME + "." + Resource.Theme.ID +
                    (dateCreate != null ? " AND ((" + Resource.Mandate.DATE_COMPLETED + " >= " + "'" + dateCreate + "'"
                            + ") OR (" + Resource.Mandate.DATE_SUCCESS + " >= " + "'" + dateCreate + "'" + "))" : "")
                    + (dateComplite != null ? " AND ((" + Resource.Mandate.DATE_COMPLETED + " <= " + "'" + dateComplite + "'"
                    + ") OR (" + Resource.Mandate.DATE_SUCCESS + " <= " + "'" + dateComplite + "'" + "))" : "")
                    , null);
            List<Mandate> userList = parseCursor(cursor);
            return userList;
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public Mandate getBy(long id) {

        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Mandate.TABLE_NAME + " where " + Resource.Mandate.ID + "= ?", new String[]{String.valueOf(id)});
            if (cursor != null) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public List<Mandate> parseCursor(Cursor cursor) {
        List<Mandate> mandateList = new ArrayList<>();
        try {
            DBOperation.open(context);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(cursor.getColumnIndex(Resource.Mandate.ID));
                    long idStatus = cursor.getLong(cursor.getColumnIndex(Resource.Mandate.STATUS));
                    String dateCompleted = cursor.getString(cursor.getColumnIndex(Resource.Mandate.DATE_COMPLETED));
                    String dateCreate = cursor.getString(cursor.getColumnIndex(Resource.Mandate.DATE_CREATE));
                    String dateSuccess = cursor.getString(cursor.getColumnIndex(Resource.Mandate.DATE_SUCCESS));
                    String status = cursor.getString(cursor.getColumnIndex(Resource.Status.NAME));
                    String description = cursor.getString(cursor.getColumnIndex(Resource.Mandate.DESCRIPTION));
                    Long userId = cursor.getLong(cursor.getColumnIndex(Resource.Mandate.USER_ID));
                    String user = cursor.getString(cursor.getColumnIndex(Resource.User.NAME));
                    long themeId = cursor.getLong(cursor.getColumnIndex(Resource.Theme.ID));
                    String theme = cursor.getString(cursor.getColumnIndex(Resource.Theme.TOPIC));

                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = new Date();
                    Date date1 = new Date();
                    Date date2 = new Date();
                    try {
                        date = formatter.parse(dateCreate);
                        date1 = formatter.parse(dateCompleted);
                        date2 = formatter.parse(dateSuccess);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    mandateList.add(new Mandate(id, new Theme(themeId, theme), date, date1, date2, new User(userId, user), new Status(idStatus, status), description));

                } while (cursor.moveToNext());

            }
            if (cursor != null) {
                cursor.close();
            }
            return mandateList;
        } finally {
            DBOperation.close();
        }
    }

    private ContentValues fullUserContentValue(Mandate mandate) {
        ContentValues cv = new ContentValues();
        UserDao userDao = new UserDao(context);
        StatusDao statusDao = new StatusDao(context);
        ThemeDao themeDao = new ThemeDao(context);
        User user = userDao.getByName(mandate.getUser());
        Status status = statusDao.getByName(mandate.getStatus());
        Theme theme = themeDao.getByName(mandate.getTheme());
        cv.put(Resource.Mandate.STATUS, status != null ? status.getId() : statusDao.save(mandate.getStatus()));
        cv.put(Resource.Mandate.USER_ID, user != null ? user.getId() : userDao.save(mandate.getUser()));
        cv.put(Resource.Mandate.THEME, theme != null ? theme.getId() : themeDao.save(mandate.getTheme()));
        cv = userContentValue(mandate, cv);
        return cv;
    }

    private ContentValues fullUserContentValueUpdate(Mandate mandate) {
        ContentValues cv = new ContentValues();
        UserDao userDao = new UserDao(context);
        ThemeDao themeDao = new ThemeDao(context);
        StatusDao statusDao = new StatusDao(context);
        User user = userDao.getByName(mandate.getUser());
        Status status = statusDao.getByName(mandate.getStatus());
        Theme theme = themeDao.getByName(mandate.getTheme());
        cv.put(Resource.Mandate.STATUS, status != null ? status.getId() : statusDao.save(mandate.getStatus()));
        cv.put(Resource.Mandate.USER_ID, user != null ? user.getId() : userDao.save(mandate.getUser()));
        cv.put(Resource.Mandate.THEME, theme != null ? theme.getId() : themeDao.save(mandate.getTheme()));
        cv = userContentValue(mandate, cv);
        return cv;
    }

    private ContentValues userContentValue(Mandate mandate, ContentValues cv) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy", Locale.getDefault());
        Log.d("MyLog", dateFormat.format(mandate.getDateCreate()) + " " + dateFormat.format(mandate.getDateCompleted()));
        cv.put(Resource.Mandate.DATE_CREATE, dateFormat.format(mandate.getDateCreate()));
        cv.put(Resource.Mandate.DATE_COMPLETED, dateFormat.format(mandate.getDateCompleted()));
        cv.put(Resource.Mandate.DESCRIPTION, mandate.getDescription());
        cv.put(Resource.Mandate.DATE_SUCCESS, String.valueOf(mandate.getDateSuccess()));
        return cv;

    }
}
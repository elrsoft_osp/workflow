package com.elrsoft.workflow.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.elrsoft.workflow.db.DBOperation;
import com.elrsoft.workflow.db.Resource;
import com.elrsoft.workflow.model.Status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by misha on 12/03/15.
 */
public class StatusDao implements Dao<Status> {

    Context context;

    public StatusDao(Context context) {
        this.context = context;
    }

    @Override
    public long save(Status status) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.insert(Resource.Status.TABLE_NAME, null, fullUserContentValue(status));
        } finally {
        }
    }

    public Map<String, Integer> getCount() {
        try {

            DBOperation.open(context);
            Cursor c = DBOperation.sqLiteDatabase.rawQuery("select " + Resource.Status.NAME
                    + " , count(" + Resource.Mandate.STATUS + ") " + "from " + Resource.Status.TABLE_NAME + " , "
                    + Resource.Mandate.TABLE_NAME + " where " + Resource.Mandate.STATUS + " = " + Resource.Status.ID
                    + " group by " + Resource.Status.NAME, null);
            Map<String, Integer> map = new HashMap<>();
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    map.put(c.getString(c.getColumnIndex("nameStatus")), c.getInt(c.getColumnIndex("count(status_id)")));
                } while (c.moveToNext());
            }
            return map;

        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public long delete(Status status) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.delete(Resource.Status.TABLE_NAME, Resource.Status.ID + " = ? ",
                    new String[]{String.valueOf(status.getId())
                    }
            );
        } finally {
        }
    }

    @Override
    public int update(Status status) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.update(Resource.Status.TABLE_NAME, fullUserContentValue(status), Resource.Status.ID + "= ?",
                    new String[]{String.valueOf(status.getId())});
        } finally {
        }
    }

    @Override
    public List<Status> get() {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Status.TABLE_NAME, null);
            List<Status> statusList = parseCursor(cursor);
            return statusList;
        } finally {
            DBOperation.close();
        }
    }

    @Override
    public Status getBy(long id) {
        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Status.TABLE_NAME + " where " + Resource.Status.ID + "= ?", new String[]{String.valueOf(id)});
            if (cursor != null) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }

        } finally {
            DBOperation.close();
        }
    }

    public Status getByName(Status status) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Status.TABLE_NAME + " where " + Resource.Status.NAME + "= ?", new String[]{String.valueOf(status.getName())});
            if (cursor != null && cursor.getCount() > 0) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }

        } finally {
        }
    }

    @Override
    public List<Status> parseCursor(Cursor cursor) {
        try {
            if (!DBOperation.sqLiteDatabase.isOpen()) {
                DBOperation.open(context);
            }
            List<Status> statusList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(cursor.getColumnIndex(Resource.Status.ID));
                    String name = cursor.getString(cursor.getColumnIndex(Resource.Status.NAME));
                    statusList.add(new Status(id, name));
                } while (cursor.moveToNext());
            }
            if (cursor != null) {
                cursor.close();
            }
            return statusList;
        } finally {
        }
    }

    private ContentValues fullUserContentValue(Status status) {
        ContentValues cv = new ContentValues();
        cv.put(Resource.Status.NAME, status.getName());
        return cv;
    }
}

package com.elrsoft.workflow.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.elrsoft.workflow.db.DBOperation;
import com.elrsoft.workflow.db.Resource;
import com.elrsoft.workflow.model.Theme;
import com.elrsoft.workflow.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 17.03.2015.
 */
public class ThemeDao implements Dao<Theme> {

    Context context;

    public ThemeDao(Context context) {
        this.context = context;
    }

    @Override
    public long save(Theme theme) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.insert(Resource.Theme.TABLE_NAME, null, fullUserContentValue(theme));
        } finally {
        }
    }

    @Override
    public long delete(Theme theme) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.delete(Resource.Theme.TABLE_NAME, Resource.Theme.ID + " = ? ",
                    new String[]{String.valueOf(theme.getId())
                    }
            );
        } finally {
        }
    }

    @Override
    public int update(Theme theme) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.update(Resource.Theme.TABLE_NAME, fullUserContentValue(theme), Resource.Theme.ID + "= ?",
                    new String[]{String.valueOf(theme.getId())});
        } finally {
        }
    }

    @Override
    public List<Theme> get() {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Theme.TABLE_NAME, null);
            List<Theme> userList = parseCursor(cursor);
            return userList;
        } finally {
            DBOperation.close();
        }
    }


    public Theme getByName(Theme theme) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Theme.TABLE_NAME + " where " + Resource.Theme.TOPIC + "= ?", new String[]{String.valueOf(theme.getTopic())});
            if (cursor != null && cursor.getCount() > 0) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }

        } finally {
        }
    }

    @Override
    public Theme getBy(long id) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.Theme.TABLE_NAME + " where " + Resource.Theme.ID + "= ?", new String[]{String.valueOf(id)});
            if (cursor != null) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }

        } finally {
        }
    }


    @Override
    public List<Theme > parseCursor(Cursor cursor) {
        try {
            DBOperation.open(context);
            List<Theme> themes = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(cursor.getColumnIndex(Resource.Theme.ID));
                    String theme = cursor.getString(cursor.getColumnIndex(Resource.Theme.TOPIC));
                    themes.add(new Theme(id, theme));

                } while (cursor.moveToNext());

            }
            if (cursor != null) {
                cursor.close();
            }
            return themes;
        } finally {
        }
    }

    private ContentValues fullUserContentValue(Theme theme) {
        ContentValues cv = new ContentValues();
        cv.put(Resource.Theme.TOPIC, theme.getTopic());
        return cv;
    }
}

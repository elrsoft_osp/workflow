package com.elrsoft.workflow.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.elrsoft.workflow.db.DBOperation;
import com.elrsoft.workflow.db.Resource;
import com.elrsoft.workflow.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by misha on 12/03/15.
 */
public class UserDao implements Dao<User> {
    Context context;

    public UserDao(Context context) {
        this.context = context;
    }

    @Override
    public long save(User user) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.insert(Resource.User.TABLE_NAME, null, fullUserContentValue(user));
        } finally {
        }
    }

    @Override
    public long delete(User user) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.delete(Resource.User.TABLE_NAME, Resource.User.ID + " = ? ",
                    new String[]{String.valueOf(user.getId())
                    }
            );
        } finally {
        }
    }

    @Override
    public int update(User user) {
        try {
            DBOperation.open(context);
            return DBOperation.sqLiteDatabase.update(Resource.User.TABLE_NAME, fullUserContentValue(user), Resource.User.ID + "= ?",
                    new String[]{String.valueOf(user.getId())});
        } finally {
        }
    }

    @Override
    public List<User> get() {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.User.TABLE_NAME, null);
            List<User> userList = parseCursor(cursor);
            return userList;
        } finally {
            DBOperation.close();
        }
    }


    public User getByName(User user) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.User.TABLE_NAME + " where " + Resource.User.NAME + "= ?", new String[]{String.valueOf(user.getName())});
            if (cursor != null && cursor.getCount() > 0) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }

        } finally {
        }
    }

    @Override
    public User getBy(long id) {
        try {
            DBOperation.open(context);
            Cursor cursor = DBOperation.sqLiteDatabase.rawQuery("select * from " + Resource.User.TABLE_NAME + " where " + Resource.User.ID + "= ?", new String[]{String.valueOf(id)});
            if (cursor != null) {
                return parseCursor(cursor).get(0);
            } else {
                return null;
            }

        } finally {
        }
    }


    @Override
    public List<User> parseCursor(Cursor cursor) {
        try {
            DBOperation.open(context);
            List<User> userList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(cursor.getColumnIndex(Resource.User.ID));
                    String name = cursor.getString(cursor.getColumnIndex(Resource.User.NAME));
                    userList.add(new User(id, name));

                } while (cursor.moveToNext());

            }
            if (cursor != null) {
                cursor.close();
            }
            return userList;
        } finally {
        }
    }

    private ContentValues fullUserContentValue(User user) {
        ContentValues cv = new ContentValues();
        cv.put(Resource.User.NAME, user.getName());
        return cv;
    }
}
package com.elrsoft.workflow.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by misha on 12/03/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, Resource.DB_NAME, null, Resource.DB_VERTION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Resource.Auth.CREATE_TABLE);
        db.execSQL(Resource.Mandate.CREATE_TABLE);
        db.execSQL(Resource.Status.CREATE_TABLE);
        db.execSQL(Resource.User.CREATE_TABLE);
        db.execSQL(Resource.Theme.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

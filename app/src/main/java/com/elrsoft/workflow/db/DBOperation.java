package com.elrsoft.workflow.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by misha on 12/03/15.
 */
public class DBOperation {

    private static DBHelper dbHelper;
    public static SQLiteDatabase sqLiteDatabase;


    public static void open(Context context) {
        if (sqLiteDatabase == null || !sqLiteDatabase.isOpen()) {
            dbHelper = new DBHelper(context);
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
    }

    public static void close() {
        if (dbHelper != null) {
            dbHelper.close();

        }
    }

}

package com.elrsoft.workflow.db;

/**
 * Created by misha on 12/03/15.
 */
public class Resource {
    public static final String DB_NAME = "db_workflow";
    public static final int DB_VERTION = 1;

    public static final class Auth {

        public static final String TABLE_NAME = "auth";
        public static final String ID = "id";
        public static final String LOGIN = "login";
        public static final String PASSWORD = "password";
        public static final String USER_ID = "user";
        public static final String CREATE_TABLE = "create table " + TABLE_NAME + " (" + ID + " integer primary key autoincrement," +
                LOGIN + " text(255), " + PASSWORD + " text(255), " + USER_ID + " text(255));";
    }

    public static final class Mandate {

        public static final String TABLE_NAME = "mandate";
        public static final String ID = "mandate_id";
        public static final String DATE_CREATE = "dateCreate";
        public static final String DATE_COMPLETED = "dateCompleted";
        public static final String USER_ID = "user";
        public static final String THEME = "treme_id";
        public static final String STATUS = "status_id";
        public static final String DESCRIPTION = "description";
        public static final String DATE_SUCCESS = "succes";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                "(" + ID + " integer primary key autoincrement, " + THEME + " int, " +
                DATE_CREATE + " Date, " + DATE_COMPLETED + " Date, " +
                USER_ID + " int, " + STATUS + " int, " +
                DESCRIPTION + " text(255), " + DATE_SUCCESS + " Date); ";


    }

    public static final class Theme {
        public static final String TABLE_NAME = "theme";
        public static final String ID = "id";
        public static final String TOPIC = "topic";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + ID + " integer primary key autoincrement, "
                + TOPIC + " text(255) unique);";
    }

    public static final class User {

        public static final String TABLE_NAME = "user";
        public static final String ID = "id";
        public static final String NAME = "nameUser";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + ID + " integer primary key autoincrement, "
                + NAME + " text(255) unique);";


    }

    public static final class Status {

        public static final String TABLE_NAME = "status";
        public static final String ID = "id";
        public static final String NAME = "nameStatus";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + ID + " integer primary key autoincrement, "
                + NAME + " text(255) unique);";

    }
}

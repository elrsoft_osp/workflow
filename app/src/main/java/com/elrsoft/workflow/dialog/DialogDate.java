package com.elrsoft.workflow.dialog;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.elrsoft.workflow.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DialogDate {

    private int mSelectedYear;
    private int mSelectedMonth;
    private int mSelectedDay;

    EditText editDate;

    public DialogDate(Activity activity, View view) {
        Calendar calendar = Calendar.getInstance();
        this.mSelectedYear = calendar.get(Calendar.YEAR);
        this.mSelectedMonth = calendar.get(Calendar.MONTH);
        this.mSelectedDay = calendar.get(Calendar.DAY_OF_MONTH);
        editDate = (EditText) view.findViewById(R.id.editDateCompletedInCreateFragment);

        showDatePickerDialog(activity, mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListener);
    }

    private OnDateSetListener mOnDateSetListener = new OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;

            String month = ((mSelectedMonth + 1) > 9) ? "" + (mSelectedMonth + 1)
                    : "0" + (mSelectedMonth + 1);
            String day = ((mSelectedDay) < 10) ? "0" + mSelectedDay : ""
                    + mSelectedDay;
            editDate.setText(day + "/" + month + "/" + mSelectedYear);


        }
    };


    private DatePickerDialog showDatePickerDialog(Activity activity,
                                                  int initialYear, int initialMonth, int initialDay,
                                                  OnDateSetListener listener) {
        DatePickerDialog dialog = new DatePickerDialog(activity, listener,
                initialYear, initialMonth, initialDay);
        dialog.show();
        return dialog;
    }
}
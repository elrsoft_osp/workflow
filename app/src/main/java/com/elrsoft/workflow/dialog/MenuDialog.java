package com.elrsoft.workflow.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.controller.BundleManagerController;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.dao.MandateDao;
import com.elrsoft.workflow.db.Resource;
import com.elrsoft.workflow.fragment.EditFragment;
import com.elrsoft.workflow.model.Mandate;

/**
 * Created by Markevych on 10.03.2015.
 */
public class MenuDialog extends DialogFragment implements View.OnClickListener {

    TextView txtEdit, txtDelete, txtCancel;

    OnFinishDialog onFinishDialog;

    public interface OnFinishDialog {
        public void isOnFinishDialog();
    }

    public void setOnFinishDialog(OnFinishDialog onFinishDialog) {
        this.onFinishDialog = onFinishDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_menu, container, false);
        getDialog().setTitle("Редагування запису");
        txtEdit = (TextView) view.findViewById(R.id.txtEdit);
        txtDelete = (TextView) view.findViewById(R.id.txtDelete);
        txtCancel = (TextView) view.findViewById(R.id.txtCancel);
        txtEdit.setOnClickListener(this);
        txtDelete.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEdit:

                EditFragment fragment = new EditFragment();
                fragment.setArguments(BundleManagerController.createBundleWithMandate((Mandate) getArguments().get("id"), "id"));
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), fragment, "Edit Fragment");
                break;
            case R.id.txtDelete:
                new MandateDao(getActivity()).delete((Mandate) getArguments().get("id"));
                onFinishDialog.isOnFinishDialog();
                break;
            case R.id.txtCancel:
                break;

        }
        getDialog().dismiss();
    }


}
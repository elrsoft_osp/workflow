package com.elrsoft.workflow.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.controller.ResourceController;
import com.elrsoft.workflow.dao.MandateDao;
import com.elrsoft.workflow.dialog.DateDialog;
import com.elrsoft.workflow.model.Mandate;
import com.elrsoft.workflow.model.Status;
import com.elrsoft.workflow.model.Theme;
import com.elrsoft.workflow.model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by misha on 10/03/15.
 */
public class CreateFragment extends Fragment implements View.OnClickListener {


    EditText editNewUser, editDateCompleted, editTheme, editCreateComment;
    Button btnSave, btnExit;
    String sUserName, sDateCompleted, sTheme, sComment;
    Date date;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.create_fragment, container, false);

        editNewUser = (EditText) view.findViewById(R.id.editNewNameInCreateFragment);
        editDateCompleted = (EditText) view.findViewById(R.id.editDateCompletedInCreateFragment);
        new DateDialog(editDateCompleted, getActivity());


        editTheme = (EditText) view.findViewById(R.id.editNewThemeInCreateFragment);
        editCreateComment = (EditText) view.findViewById(R.id.editNewCommentInCreateFragment);

        btnSave = (Button) view.findViewById(R.id.btnAddInCreateFragment);
        btnExit = (Button) view.findViewById(R.id.btnExitInCreateFragment);
        btnSave.setOnClickListener(this);
        btnExit.setOnClickListener(this);

        return view;

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnAddInCreateFragment:

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                sUserName = editNewUser.getText().toString();
                sDateCompleted = editDateCompleted.getText().toString();
                sTheme = editTheme.getText().toString();
                sComment = editCreateComment.getText().toString();

                try {
                    date = formatter.parse(sDateCompleted);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (!editNewUser.getText().toString().equals("") && !editDateCompleted.getText().toString().equals("") && !editTheme.getText().toString().equals("")) {
                    Mandate mandate = new Mandate(new Theme(sTheme), Calendar.getInstance().getTime(), date, null, new User(sUserName), new Status("В роботі"), sComment);
                    new MandateDao(getActivity()).save(mandate);
                    ResourceController.toast(getActivity(), R.string.add);
                    FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
                } else {
                    ResourceController.toast(getActivity(), R.string.all_fields);
                }
                break;
            case R.id.btnExitInCreateFragment:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
                break;
        }
    }
}
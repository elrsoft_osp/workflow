package com.elrsoft.workflow.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.adapter.SpinnerAdapter;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.controller.ResourceController;
import com.elrsoft.workflow.dao.MandateDao;
import com.elrsoft.workflow.dialog.DateDialog;
import com.elrsoft.workflow.model.Mandate;
import com.elrsoft.workflow.model.Status;
import com.elrsoft.workflow.model.Theme;
import com.elrsoft.workflow.model.User;

import java.util.Calendar;

/**
 * Created by misha on 10/03/15.
 */
public class EditFragment extends Fragment implements View.OnClickListener {

    EditText redactNewName, redactDateCompleted, redactNewThemet, redactNewComment;
    Button redactBtnExit, redactBtnAdd;
    Spinner status;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_fragment, null, false);
        redactNewName = (EditText) view.findViewById(R.id.redactNewName);
        redactDateCompleted = (EditText) view.findViewById(R.id.redactDateCompleted);
        new DateDialog(redactDateCompleted, getActivity());
        redactNewThemet = (EditText) view.findViewById(R.id.redactNewTheme);
        redactNewComment = (EditText) view.findViewById(R.id.redactNewComment);

        status = (Spinner) view.findViewById(R.id.status);
        status.setAdapter(new SpinnerAdapter(ResourceController.getAllStatus(), getActivity()));

        redactBtnAdd = (Button) view.findViewById(R.id.redactBtnAdd);
        redactBtnAdd.setOnClickListener(this);

        redactBtnExit = (Button) view.findViewById(R.id.redactBtnExit);
        redactBtnExit.setOnClickListener(this);

        if (getArguments() != null) {
            Mandate mandate = (Mandate) getArguments().get("id");
            redactNewName.setText(mandate.getUser().getName());
            redactNewThemet.setText(mandate.getTheme().getTopic());
            redactDateCompleted.setText("" + DateFormat.format("dd/MM/yyyy", mandate.getDateCompleted()));
            redactNewComment.setText(mandate.getDescription());

        }
        return view;

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.redactBtnAdd:
                if (!redactNewThemet.getText().toString().equals("") && !redactNewName.getText().toString().equals("") && !redactDateCompleted.getText().toString().equals("")) {
                    Mandate oldMandate = (Mandate) getArguments().get("id");

                    Mandate mandate = new Mandate(oldMandate.getId(), new Theme(oldMandate.getTheme().getId(), redactNewThemet.getText().toString()),
                            oldMandate.getDateCreate(), oldMandate.getDateCompleted(),
                            status.getSelectedItem().toString().equals("Виконано") ? Calendar.getInstance().getTime() : null,
                            new User(oldMandate.getUser().getId(), redactNewName.getText().toString()),
                            new Status(oldMandate.getStatus().getId(), (String) status.getSelectedItem()),
                            redactNewComment.getText().toString());

                    new MandateDao(getActivity()).update(mandate);
                    FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
                    ResourceController.toast(getActivity(), R.string.update);
                } else {
                    ResourceController.toast(getActivity(), R.string.all_fields);
                }
                break;


            case R.id.redactBtnExit:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
                break;
        }
    }
}
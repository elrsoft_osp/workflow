package com.elrsoft.workflow.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.adapter.SpinnerAdapter;
import com.elrsoft.workflow.controller.BundleManagerController;
import com.elrsoft.workflow.controller.ConvertController;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.dao.MandateDao;
import com.elrsoft.workflow.dao.StatusDao;
import com.elrsoft.workflow.dao.ThemeDao;
import com.elrsoft.workflow.dao.UserDao;
import com.elrsoft.workflow.dialog.DateDialog;
import com.elrsoft.workflow.model.Mandate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by misha on 10/03/15.
 */
public class FilterFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    EditText filterDate;
    Spinner filterName, filterTheme, filterCondition;
    CheckBox all, favorit;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_fragment, null, false);

        view.findViewById(R.id.redactBtnAdd).setOnClickListener(this);
        view.findViewById(R.id.redactBtnExit).setOnClickListener(this);


        filterName = (Spinner) view.findViewById(R.id.spinner);
        filterName.setAdapter(new SpinnerAdapter(
                ConvertController.convertUserToString(new UserDao(getActivity()).get()), getActivity()));

        filterTheme = (Spinner) view.findViewById(R.id.filterTheme);

        filterTheme.setAdapter(new SpinnerAdapter(
                ConvertController.convertThemeToString(new ThemeDao(getActivity()).get()), getActivity()));

        filterDate = (EditText) view.findViewById(R.id.filterDate);
        new DateDialog(filterDate, getActivity());

        filterCondition = (Spinner) view.findViewById(R.id.filterСondition);
        filterCondition.setAdapter(new SpinnerAdapter(ConvertController.convertStatusToString(new StatusDao(getActivity())
                .get()), getActivity()));

        all = (CheckBox) view.findViewById(R.id.filterAllCheckBox);
        favorit = (CheckBox) view.findViewById(R.id.filterSelectCheckBox);

        favorit.setChecked(true);
        all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setEnable(!isChecked);
                favorit.setChecked(!isChecked);
            }
        });

        favorit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                all.setChecked(!isChecked);
                setEnable(isChecked);
                favorit.setChecked(isChecked);

            }
        });

        return view;
    }

    private void setEnable(boolean flag) {
        filterName.setEnabled(flag);
        filterTheme.setEnabled(flag);
        filterDate.setEnabled(flag);
        filterCondition.setEnabled(flag);
        favorit.setChecked(flag);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.redactBtnAdd:
                if (all.isChecked()) {
                    FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
                } else if (favorit.isChecked()) {
                    List<Mandate> mandateList = new MandateDao(getActivity()).getMandateWithSearge(filterName.getSelectedItem().toString().equals("") ? null : filterName.getSelectedItem().toString(), filterDate.getText().toString().equals("") ? null : filterDate.getText().toString(), filterTheme.getSelectedItem().toString().equals("") ? null : filterTheme.getSelectedItem().toString(), filterCondition.getSelectedItem().toString().equals("") ? null : filterCondition.getSelectedItem().toString());
                    Bundle bundle = BundleManagerController.createBundleWithMandateList((ArrayList) mandateList, "id");
                    MainFragment fragment = new MainFragment();
                    fragment.setArguments(bundle);
                    FragmentNavigationManagerController.navigationManagerControlle(getActivity(), fragment, getString(R.string.main));
                }
                break;
            case R.id.redactBtnExit:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


//        switch (view.getId()){
//            case R.id.spinner:
//                strSpinnerName = parent.getItemAtPosition(position).toString();
//                break;
//            case R.id.filterTheme:
//                strSpinnerTheme = parent.getItemAtPosition(position).toString();
//                break;
//            case R.id.filterСondition:
//                strSpinnerStatus = parent.getItemAtPosition(position).toString();
//                break;
//        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
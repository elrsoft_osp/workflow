package com.elrsoft.workflow.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.adapter.MandateAdapter;
import com.elrsoft.workflow.controller.BundleManagerController;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.dao.MandateDao;
import com.elrsoft.workflow.dialog.MenuDialog;
import com.elrsoft.workflow.model.Mandate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by misha on 10/03/15.
 */

public class MainFragment extends Fragment {

    MandateAdapter viewAdapter;
    List<Mandate> mandateList = new ArrayList<>();
    RecyclerView recyclerView;
    View data_no_found;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, null, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.listViewMandate);
        data_no_found = view.findViewById(R.id.data_no_found);
        updateAdapter();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    private void updateAdapter() {

        if (getArguments() != null) {
            mandateList = (List<Mandate>) getArguments().get("id");
        } else {
            mandateList = new MandateDao(getActivity()).get();
        }


        if (mandateList.size() > 0) {
            viewAdapter = new MandateAdapter(this, mandateList);
            viewAdapter.setOnItemClickListener(new MandateAdapter.OnItemLongClickListener() {
                @Override
                public void onLongClick(Mandate mandate) {
                    MenuDialog menuDialog = new MenuDialog();
                    menuDialog.setArguments(BundleManagerController.createBundleWithMandate(mandate, "id"));
                    menuDialog.setOnFinishDialog(new MenuDialog.OnFinishDialog() {
                        @Override
                        public void isOnFinishDialog() {
                            updateAdapter();
                        }
                    });
                    menuDialog.show(getFragmentManager(), "Dialog");
                }

            });
            recyclerView.setAdapter(viewAdapter);
        } else {
            viewAdapter = new MandateAdapter(this, mandateList);
            recyclerView.setAdapter(viewAdapter);
            data_no_found.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newTask:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new CreateFragment(), getString(R.string.tag_create_fragment));
                break;
            case R.id.statistics:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new StatisticsFragment(), getString(R.string.tag_statistics_fragment));
                break;
            case R.id.rating:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new ViewStatisticsFragment(), getString(R.string.tag_view_statistics_fragment));
                break;
            case R.id.filter:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new FilterFragment(), getString(R.string.tag_filter_fragment));
                break;
            case R.id.exitMenu:
                getActivity().finish();
                break;
        }

        return true;
    }
}
package com.elrsoft.workflow.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.adapter.SpinnerAdapter;
import com.elrsoft.workflow.controller.BundleManagerController;
import com.elrsoft.workflow.controller.ConvertController;
import com.elrsoft.workflow.controller.DateManagerController;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.dao.MandateDao;
import com.elrsoft.workflow.dao.UserDao;
import com.elrsoft.workflow.dialog.DateDialog;
import com.elrsoft.workflow.model.Mandate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by misha on 10/03/15.
 */
public class StatisticsFragment extends Fragment implements View.OnClickListener {

    EditText editDateFrom, editDateTo;
    Spinner editNameUser;
    Button btnFind, btnExit;
    CheckBox toDay, nextDay;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.statistics_fragment, null, false);

        editNameUser = (Spinner) view.findViewById(R.id.editNameUserInStatistics);

        editNameUser.setAdapter(new SpinnerAdapter(ConvertController.convertUserToString(new UserDao(getActivity()).get()), getActivity()));
        editDateFrom = (EditText) view.findViewById(R.id.editDateFromInStatistics);
        editDateTo = (EditText) view.findViewById(R.id.editDateToInStatistics);
        new DateDialog(editDateFrom, getActivity());
        new DateDialog(editDateTo, getActivity());

        toDay = (CheckBox) view.findViewById(R.id.checkboxToday);
        toDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableAll(!isChecked);
                nextDay.setEnabled(!isChecked);
            }
        });

        nextDay = (CheckBox) view.findViewById(R.id.checkboxnextDay);
        nextDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableAll(!isChecked);
                toDay.setEnabled(!isChecked);
            }
        });

        new DateDialog(editDateFrom, getActivity());
        new DateDialog(editDateTo, getActivity());

        btnFind = (Button) view.findViewById(R.id.btnFindInStatistics);
        btnExit = (Button) view.findViewById(R.id.btnExitInStatistics);
        btnFind.setOnClickListener(this);
        btnExit.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFindInStatistics:
                List<Mandate> mandates = new ArrayList<>();
                if (toDay.isChecked()) {
                    mandates = new MandateDao(getActivity()).getFilterToday(DateFormat.format("dd/MM/yyyy", Calendar.getInstance().getTime()).toString(), DateFormat.format("dd/MM/yyyy", Calendar.getInstance().getTime()).toString());
                } else if (nextDay.isChecked()) {
                    Date date = Calendar.getInstance().getTime();
                    mandates = new MandateDao(getActivity()).getFilterNextDay(DateFormat.format("dd/MM/yyyy", DateManagerController.getDateInNextDays(date, 1)).toString(), DateFormat.format("dd/MM/yyyy", DateManagerController.getDateInNextDays(date, 1)).toString());
                } else {
                    mandates = new MandateDao(getActivity()).getFilter(editDateFrom.getText().toString().equals("") ? null : editDateFrom.getText().toString(), editDateTo.getText().toString().equals("") ? null : editDateTo.getText().toString(), ((String) editNameUser.getSelectedItem()).equals("") ? null : (String) editNameUser.getSelectedItem());
                }
                Bundle bundle = BundleManagerController.createBundleWithMandateList((ArrayList) mandates, "id");
                MainFragment fragment = new MainFragment();
                fragment.setArguments(bundle);
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), fragment, getString(R.string.main));
                break;
            case R.id.btnExitInStatistics:
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
                break;
        }
    }

    private void enableAll(boolean flag) {
        editDateFrom.setEnabled(flag);
        editDateTo.setEnabled(flag);
    }
}
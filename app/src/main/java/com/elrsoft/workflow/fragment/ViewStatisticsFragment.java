package com.elrsoft.workflow.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.controller.FragmentNavigationManagerController;
import com.elrsoft.workflow.dao.StatusDao;
import com.elrsoft.workflow.view.PercertView;

/**
 * Created by misha on 10/03/15.
 */
public class ViewStatisticsFragment extends Fragment {

    Button btnExit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_statistics_fragment, null, false);

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.conteinerLiner);
        PercertView percertView = new PercertView(getActivity(), new StatusDao(getActivity()).getCount());
        linearLayout.addView(percertView);

        btnExit = (Button) view.findViewById(R.id.btnExitInViewStatistics);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentNavigationManagerController.navigationManagerControlle(getActivity(), new MainFragment(), getString(R.string.main));
            }
        });
        return view;
    }
}
package com.elrsoft.workflow.model;

/**
 * Created by misha on 10/03/15.
 */
public class Authorization {
    private long id;
    private String login;
    private String password;
    private User user;

    public Authorization() {

    }

    public Authorization(long id, String login, String password, User user) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.user = user;
    }

    public Authorization(String login, String password, User user) {
        this.login = login;
        this.password = password;
        this.user = user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

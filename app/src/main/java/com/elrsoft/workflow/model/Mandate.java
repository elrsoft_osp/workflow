package com.elrsoft.workflow.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by misha on 10/03/15.
 */
public class Mandate implements Parcelable {
    private long id;
    private Date dateCreate;
    private Date dateCompleted;
    private Date dateSuccess;
    private User user;
    private Theme theme;
    private Status status;
    private String description;

    public Mandate() {

    }

    public Mandate(User user, Theme theme, Date dateCompleted) {
        this.user = user;
        this.theme = theme;
        this.dateCompleted = dateCompleted;
    }

    public Mandate(User user, Theme theme, Date dateCreate, Date dateCompleted) {
        this.user = user;
        this.theme = theme;
        this.dateCreate = dateCreate;
        this.dateCompleted = dateCompleted;
    }


    public Mandate(long id, Theme theme, Date dateCreate, Date dateCompleted, Date dateSuccess, User user, Status status, String description) {
        this.id = id;
        this.theme = theme;
        this.dateCreate = dateCreate;
        this.dateCompleted = dateCompleted;
        this.dateSuccess = dateSuccess;
        this.user = user;
        this.status = status;
        this.description = description;

    }

    public Mandate(Theme theme, Date dateCreate, Date dateCompleted, Date dateSuccess, User user, Status status, String description) {
        this.theme = theme;
        this.dateCreate = dateCreate;
        this.dateCompleted = dateCompleted;
        this.dateSuccess = dateSuccess;
        this.user = user;
        this.status = status;
        this.description = description;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public Date getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Date dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateSuccess() {
        return dateSuccess;
    }

    public void setDateSuccess(Date dateSuccess) {
        this.dateSuccess = dateSuccess;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeLong(id);
        dest.writeParcelable(theme, flags);
        dest.writeSerializable(dateCreate);
        dest.writeSerializable(dateCompleted);
        dest.writeSerializable(dateSuccess);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(status, flags);
        dest.writeString(description);

    }

    public static final Creator<Mandate> CREATOR = new Creator<Mandate>() {
        @Override
        public Mandate createFromParcel(Parcel source) {
            return new Mandate(source);
        }

        @Override
        public Mandate[] newArray(int size) {
            return new Mandate[size];
        }
    };

    private Mandate(Parcel parcel) {

        id = parcel.readLong();
        theme = (Theme) parcel.readParcelable(Theme.class.getClassLoader());
        dateCreate = (Date) parcel.readSerializable();
        dateCompleted = (Date) parcel.readSerializable();
        dateSuccess = (Date) parcel.readSerializable();
        user = (User) parcel.readParcelable(User.class.getClassLoader());
        status = (Status) parcel.readParcelable(Status.class.getClassLoader());
        description = parcel.readString();
    }
}

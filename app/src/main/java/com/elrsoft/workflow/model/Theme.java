package com.elrsoft.workflow.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by <ELRsoft> HANZ on 17.03.2015.
 */
public class Theme implements Parcelable {

    long id;
    String topic;

    public Theme(long id, String topic) {
        this.id = id;
        this.topic = topic;
    }

    public Theme(String topic) {
        this.topic = topic;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(topic);
    }

    public static final Creator<Theme> CREATOR = new Creator<Theme>() {
        @Override
        public Theme createFromParcel(Parcel source) {
            return new Theme(source);
        }

        @Override
        public Theme[] newArray(int size) {
            return new Theme[size];
        }
    };

    private Theme(Parcel parcel) {
        id = parcel.readLong();
        topic = parcel.readString();
    }
}

package com.elrsoft.workflow.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.elrsoft.workflow.R;

import java.util.Map;

/**
 * Created by <ELRsoft> HANZ on 13.03.2015.
 */
public class PercertView extends View {

    Map<String, Integer> map;

    public PercertView(Context context, Map<String, Integer> map) {
        super(context);
        this.map = map;
        init();
    }

    public PercertView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PercertView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(getContext().getResources().getColor(R.color.red));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        bgpaint = new Paint();
        bgpaint.setColor(getContext().getResources().getColor(R.color.green));
        bgpaint.setAntiAlias(true);
        bgpaint.setStyle(Paint.Style.FILL);
        yelpaint = new Paint();
        yelpaint.setColor(getContext().getResources().getColor(R.color.yel));
        yelpaint.setAntiAlias(true);
        yelpaint.setStyle(Paint.Style.FILL);
        rect = new RectF();
    }

    Paint paint;
    Paint bgpaint;
    Paint yelpaint;
    RectF rect;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //draw background circle anyway
        int left = 0;
        int width = getWidth();
        int top = 0;
        float temp = 0;
        for (int i = 0; i < map.keySet().size(); i++) {
            temp += map.get(map.keySet().toArray()[i]);
        }
        temp = 360 / temp;
        rect.set(left, top, left + width, top + width);

        if (map.keySet().size() == 1) {
            Log.d("temp", "temp " + map.get(map.keySet().toArray()[0]));
            if (map.keySet().toArray()[0].equals("Виконано")) {
                canvas.drawArc(rect, 0, temp * map.get("Виконано"), true, bgpaint);
            } else {
                if (map.keySet().toArray()[0].equals("Не виконано"))
                    canvas.drawArc(rect, 0, temp * map.get("Не виконано"), true, paint);
            }
            if (map.keySet().toArray()[0].equals("В роботі")) {

                canvas.drawArc(rect, 0, temp * map.get("В роботі"), true, yelpaint);

            }
        } else {
            if (map.keySet().size() == 2) {
                if (map.keySet().toArray()[0].equals("Виконано") && map.keySet().toArray()[1].equals("Не виконано")) {
                    canvas.drawArc(rect, 0, temp * map.get("Виконано"), true, bgpaint);
                    canvas.drawArc(rect, temp * map.get("Виконано"), temp * map.get("Не виконано"), true, paint);
                } else {
                    if (map.keySet().toArray()[0].equals("Виконано") && map.keySet().toArray()[1].equals("В роботі")) {
                        canvas.drawArc(rect, 0, temp * map.get("Виконано"), true, bgpaint);
                        canvas.drawArc(rect, temp * map.get("Виконано"), temp * map.get("В роботі"), true, yelpaint);
                    } else {
                        if (map.keySet().toArray()[0].equals("Не виконано") && map.keySet().toArray()[1].equals("В роботі")) {
                            canvas.drawArc(rect, 0, temp * map.get("Не виконано"), true, paint);
                            canvas.drawArc(rect, temp * map.get("Не виконано"), temp * map.get("В роботі"), true, yelpaint);

                        }
                    }
                }

            } else {
                if (map.keySet().size() == 3) {
                    if (map.keySet().toArray()[0].equals("Виконано") && map.keySet().toArray()[1].equals("Не виконано") && map.keySet().toArray()[2].equals("В роботі")) {
                        canvas.drawArc(rect, 0, temp * map.get("Виконано"), true, bgpaint);
                        canvas.drawArc(rect, temp * map.get("Не виконано"), temp * map.get("В роботі"), true, paint);
                        canvas.drawArc(rect, temp * map.get("В роботі") + temp * map.get("Не виконано"), temp * map.get("Виконано"), true, yelpaint);

                    }
                }
            }
        }
    }
}
